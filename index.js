
const FIRST_NAME = "DAIANA";
const LAST_NAME = "AVRAM";
const GRUPA = "1090";

/**
 * Make the implementation here
 */
function initCaching() {
   /* functie care sa returneze un dictionar gol */


    function getCache() {
       return cacheValue;
    }
    function pageAccessCounter(pageName="home"){
        if(typeof(cacheValue[pageName]) !== "undefined"){
            cacheValue[pageName]++;
        }else{
            cacheValue[pageName]=1;
        }
    }

    
    
    
    var cacheValue = new Object();
    var cache = {
   getCache: getCache,
   pageAccessCounter:pageAccessCounter
}



return cache;


};






module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}